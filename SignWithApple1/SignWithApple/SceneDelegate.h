//
//  SceneDelegate.h
//  SignWithApple
//
//  Created by dayan on 2020/4/29.
//  Copyright © 2020 dayan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

