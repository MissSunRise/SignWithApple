//
//  SignInApple.h
//  SignWithApple
//
//  Created by dayan on 2020/4/29.
//  Copyright © 2020 dayan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignInApple : NSObject

+ (SignInApple *)shareInstance;

// 处理授权
- (void)handleAuthorizationAppleIDButtonPress;

@end

NS_ASSUME_NONNULL_END
